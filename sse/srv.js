let http = require('http')
let fileSystem = require('fs')
let path = require('path')

http.createServer((req, res)=>{

	switch(req.url) {
		case '/':
			let filePath = path.join(__dirname, 'index.html');
			let stat = fileSystem.statSync(filePath);

			res.writeHead(200, {
				'Content-Type': 'text/html',
				'Content-Length': stat.size
			});
			var readStream = fileSystem.createReadStream(filePath);

			readStream.pipe(res);
			break;

		case '/api':
			res.writeHead(200, {
				'Content-Type': 'text/event-stream',
				Connection: 'keep-alive',
				'Cache-Control': 'no-cache'
			})

			for(let i = 0; i < 10; i++) {

				setTimeout(()=>{
					const stamp = new Date().getTime()
					res.write('event: message\n')
					res.write(`data: api result ${stamp}\n\n`)
				},1000*i)
			}
			
			for(let j = 0; j < 10; j++) {
				setTimeout(()=>{
					const stamp = new Date().getTime()
					res.write('event: othermessage\n')
					res.write(`data: other data ${stamp}\n\n`)
				}, 1400*j)
			}
			break;

	}
	
}).listen(3000)
