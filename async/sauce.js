console.log('first script')

const func1 = async () => {
  return new Promise(resolve => {
    setTimeout(()=>{
      console.log('i timeout')
      resolve()
    },1000)
  })
}

const func2 = async () => {
  await func1()
  console.log('starting second script')
  window.go = true
}

func2()