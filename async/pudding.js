
console.log('second script')

const sleep = (time)=> {
  return new Promise(resolve => {
    setTimeout(()=>{
      resolve()
    },time)
  })
}


const go = async () => {
  let wait = 100

  for(let i = 0; i < wait && !window.go; i++) {
    console.log('waiting')
    await sleep(100)
  }

  console.log('borde vara sist')
}

go()
