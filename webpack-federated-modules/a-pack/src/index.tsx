
import * as React from 'react'
import * as ReactDOM from 'react-dom';

import ExportedComponent from './ExportedComponent'

ReactDOM.render(
  (<ExportedComponent />),
  document.getElementById('root')
)