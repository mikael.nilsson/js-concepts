const HtmlWebpackPlugin = require('html-webpack-plugin')
const ModuleFederationPlugin = require('webpack').container.ModuleFederationPlugin;
const path = require('path')

module.exports = {
  entry: './src/index',
  mode: 'development',  
  devServer: {
    host: 'localhost',
    port: 4001,
    historyApiFallback: true,
    open: true
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: 'auto',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: ['@babel/preset-react', '@babel/preset-typescript'],
        },
      }
    ]
  },
  plugins: [
    
    new ModuleFederationPlugin({
      name: 'apack',
      filename: 'remoteEntryPoint.js',
      exposes: {
        './ExportedComponent': './src/ExportedComponent'
      }
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),
  ],
};  