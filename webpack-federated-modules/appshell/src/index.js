
import React from 'react';
import ReactDOM from 'react-dom';

import {ExportedComponent} from './exportedModule'

ReactDOM.render(
  <div>
    <p>Appshell</p>
    <ExportedComponent />
  </div>,
  document.getElementById('root')
)