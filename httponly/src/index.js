const http = require('http')
const fs = require('fs')

const getFile = async (filePath) => {
  console.log('get file')
  let stream = null
  fs.exists(filePath, (exists) => {
    if(exists) {
      console.log('creating stream')
      stream = fs.createReadStream(filePath)
      return stream
    }
  }) 
}

const parseCookies = (request) => {
    const list = {};
    const cookieHeader = request.headers?.cookie;
    if (!cookieHeader) return list;

    cookieHeader.split(`;`).forEach(function(cookie) {
        let [ name, ...rest] = cookie.split(`=`);
        name = name?.trim();
        if (!name) return;
        const value = rest.join(`=`).trim();
        if (!value) return;
        list[name] = decodeURIComponent(value);
    });

    return list;
}


const server = http.createServer((request, response) => {
  const cookies = parseCookies(request);

  switch(request.url) {
    case '/login':
      console.log('do some login magix')
      response.writeHead(200, {
        'Set-Cookie': [
          "thesecret=encryptedstuff; HttpOnly",
          "spamcookie=likespamvaluesorsmtn",
          "cartcookie={'petType':041,'breedType':'A11'}"
        ]
      })
  
      response.end()
      break

    case '/':
      let indexPath = './src/publicIndex.html'

      if(cookies.hasOwnProperty("thesecret")) {  
        // Logged in index
             
        console.log('secrets!')
        
        indexPath = './src/index.html'      
      } else {
        // logged out index

        console.log('logged out layout (lol ;-D)')
      }

      const fileStats = fs.statSync(indexPath)
      
      console.log('streaming file')
      fs.createReadStream(indexPath).pipe(response)

      break
  }

})

server.listen(2000)
