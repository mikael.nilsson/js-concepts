const i = require('./index.js');

test('returns number', ()=>{
  const result = i.getVal();
  
  expect(typeof result).toBe('number');
});