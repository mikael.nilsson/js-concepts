type TWorkerMess = number[];

const message = (event: MessageEvent<TWorkerMess>) => {
  console.log("🐝 Worker: Message received from main script");
  const data = event.data;
  const result = data[0] + data[1];

  const workerResult = "Result: " + result;
  console.log("🐝 Worker: Posting message back to main script");
  postMessage(workerResult);
};

addEventListener("message", message);

addEventListener("install", (event) => {
  console.log("worker installed");
});

addEventListener("activate", (event) => {
  console.log("activation");
});
