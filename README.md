# js-concepts

Either stuff I know or stuff I learn, I needed somewhere to stuff concepts and smaller experiments. That's here.

## react-router

Some simple experimets around Reacts router functionality

## rpc

Played around with specifically sending functions between nodes via rpc.

## react-update-array

Had some problems with handling arrays in a React form, so I needed to strip away all the other stuff to see what I was doing.

## react-jest

Needed to isolate some concepts around a react-jest-test. This one could be even smaller, I just copied a few files from another project.

## Vue hooks

My vue hooks didn't do what I wanted them to, so I created a project that only logs when each hook runs, just to have something to compare to

## Srcset

We discussed the srcset property of the img html element so I just felt I needed to do a simple test, just to make sure it was as simple as I thought. It was :).

## Webpack federated modules

We tried out federated modules for replacing our systemjs based appshell/microfrontend architecture. Didn't work out this time but we still have hopes for the future

## React Base

I needed some kind of template for really small react thingies, so I built one

## worker

I was pondering using a service worker in a nextjs-project, so had to try it out

## npmapp

Trying out the monorepo management capabilities of npm 7

## httponly
We were discussing cookies, httponly cookies and localstorage, so I needed something to talk around

## SSE
I saw the concept of Server Side Events and felt I wanted a tiny example to grab ahold of when/if the need arises
